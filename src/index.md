---
template: landing.html

title: Duke Research Computing
subtitle: Flexible computing resources matched to your research
desc: >-
  Duke Research Computing offers flexible computing resources matched to your research. General services are managed through Research Toolkits and are available for use with public and restricted data

nav_links:
    - name: sep
    - name: services
      url: '#services'
      icon: wrench
    - name: info
      url: '#info'
      icon: info
    - name: sep
    - name: status
      url: 'https://status.oit.duke.edu'
      icon: traffic-light
#      post: >-
#        <span id="status-widget"/>
    - name: news
      url: '.'
      icon: newspaper
#      post: >-
#        <span id="news-widget"/>
    - name: docs
      url: './docs'
      icon: book
    - name: compute
      url: 'https://rtoolkits.web.duke.edu/'
      icon: terminal
    - name: sep
    - name: support
      url: '#'
      icon: question-circle
    - name: slack
      url: '.'
      icon: brand-slack
    - name: sep

buttons_info: More information
buttons_account: Request an account

services_title: Services
services_subtext: A one-stop shop for all our scientific computing needs
services:
    - title: compute
      icon: server
      text: >-
         All the resources you need in one place: compute nodes, GPUs, large
         memory nodes, blazing fast interconnect, parallel filesystems, and
         more!
    - title: explore
      icon: search
      text: >-
         Duke Research Computing provides all the software tools and storage resources you'll
         need to explore and analyze your research data.
    - title: discover
      icon: flask
      text: >-
         With a whole range of computational tools at your fingertips,
         scientific breakthroughs will just be a batch job away.

info_title: In a nutshell
info_subtext: All about Duke Research Computing

---

### What is Duke Research Computing?

Duke Research Computing is a shared computing cluster available for use by all Duke
Faculty members and their research teams, for sponsored or departmental faculty
research.  All research teams on Duke Research Computing have access to a base set of managed
computing resources, GPU-based servers, and a multi-petabyte, high-performance
parallel file system for short-term storage.

Faculty can supplement these shared nodes by purchasing additional servers, and
become Duke Research Computing owners. By investing in the cluster, PI groups not only receive
exclusive access to the nodes they purchase, but also get access to all of the
other owner compute nodes when they're not in use, thus giving them access to
the whole breadth of Duke Research Computing resources.


### Why should I use Duke Research Computing?

Using Duke Research Computing for your work provides many advantages over individual
solutions: hosted in an on-premises, state-of-the-art datacenter dedicated to
research computing systems, the Duke Research Computing cluster is powered and cooled by
installations that are optimized for scientific computing.

On Duke Research Computing, simulations and workloads benefit from performance levels that
only large scale HPC systems can offer: high-performance I/O infrastructure,
petabytes of storage, large variety of hardware configurations, GPU
accelerators, centralized system administration and management provided by the
[Duke Research Computing Center][url_srcc] (SRCC).

Such features are not easily accessible at the departmental level, and often
require both significant initial investments and recurring costs. Joining
Duke Research Computing allows researchers and Faculty members to avoid those costs and
benefit from economies of scale, as well as to access larger, professionally
managed computing resources that what would not be available on an individual
or even departmental basis.


### How much does it cost?

**Duke Research Computing is free to use for anyone doing sponsored research at Duke.**

Any Faculty member can request access for research purposes, and get an account
with a base storage allocation and unlimited compute time on the global, shared
pool of resources.

Duke Research Computing provides faculty with the [opportunity to
purchase][url_purchase] from a [catalog a recommended compute node
configurations][url_catalog], for the use of their research teams. Using a
traditional compute cluster condominium model, participating faculty and their
teams get priority access to the resources they purchase. When those resources
are idle, other "owners" can use them, until the purchasing owner wants to use
them. When this happens, those other owners jobs are re-queued to free up
resources. Participating owner PIs also have shared access to the original base
Duke Research Computing nodes, along with everyone else.


### How big is it?

{% set public_cores = facts | selectattr("name", "==", "partitions")
                              | map(attribute="fields") | first
                              | rejectattr("name", "==", "owners")
                              | sum(attribute="cores")
                              | round(-2, "floor") | int %}

{% set owner_cores = facts    | selectattr("name", "==", "partitions")
                              | map(attribute="fields") | first
                              | selectattr("name", "==", "owners")
                              | map(attribute="cores") | first
                              | round(-2, "floor") | int %}

{% set pflops = facts         | selectattr("name", "==", "computing")
                              | map(attribute="fields") | first
                              | selectattr("name", "==", "PFLOPs")
                              | map(attribute="value") | first
                              | round(1, "floor") %}



Quite big! It's actually difficult to give a definitive answer, as Duke Research Computing is
constantly evolving and expanding with new hardware additions.

As of {{ git.date.strftime("%B %Y") }}, Duke Research Computing features over {{
"{:,}".format(public_cores) }} CPU cores available to all researchers, and more
than {{ "{:,}".format(owner_cores) }} additional CPU cores available to
Duke Research Computing owners, faculty who have augmented the cluster with their own
purchases. With a computing power over {{ pflops }} Petaflops, Duke Research Computing would
have its place in the Top500 list of the 500 most powerful computer systems in
the world.

For more details about Duke Research Computing size and technical specifications, please refer
to the [tech specs][url_specs] section of the documentation. And for even more
numbers and figures, see the [Duke Research Computing facts][url_facts] page.


### OK, I'm sold, how do I start?

You can request an account right now, take a look at the
[documentation](/docs), and drop us an [email](mailto:{{ support_email }}) if
you have any questions.


### I want my own nodes!

If you're interested in becoming an owner on Duke Research Computing, and benefit from all the
advantages associated, please take a look at the [catalog][url_catalog] of
configurations, feel free to use the [ordering form][url_order] to submit
your request, and we'll get back to you.


[comment]: #  (link URLs -----------------------------------------------------)

[url_srcc]:     //srcc.stanford.edu
[url_purchase]: /docs/overview/orders/
[url_catalog]:  /catalog
[url_order]:    /order
[url_specs]:    /docs/overview/tech/specs
[url_facts]:    /docs/overview/tech/facts
